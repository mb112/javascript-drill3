function filter(elements, cb) {
    let x =[]
    if (Array.isArray(elements) && elements !== []) {
        for (let i = 0; i < elements.length; i++) {
            if (cb(elements[i])) {
                x.push(elements[i])
            }
        }
    }
    return x
}

module.exports = filter