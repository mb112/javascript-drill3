function map(elements, cb) {
    let x = []
    if (Array.isArray(elements) && elements !== []) {
        for (let i = 0; i < elements.length; i++) {
            x.push(cb(elements[i]))
        }
    }
    return x
}

module.exports = map