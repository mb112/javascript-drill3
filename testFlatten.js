const sampleArray = require('./arrays')
const flatten = require('./flatten')

console.log('Test case 1 =',flatten(sampleArray.nestedArray))
console.log('Test case 2 =',flatten(sampleArray.items))
console.log('Test case 3 =',flatten([]))
console.log('Test case 4 =',flatten(null))

