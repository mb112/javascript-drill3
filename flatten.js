function flatten(elements) {
    let numbers = []
    if (Array.isArray(elements) && elements !== []) {
        for (let i = 0; i < elements.length; i++) {
            if (Array.isArray(elements[i])) {
                numbers = numbers.concat(flatten(elements[i]))
            } else {
                numbers.push(elements[i])
            }
        }

    }
    return numbers
}

module.exports = flatten

