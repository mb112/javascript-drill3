const sampleArray = require('./arrays')
const reduce = require('./reduce')



console.log('Test case 1 =',reduce(sampleArray.items, function (a, b) {return a + b}, 5))
console.log('Test case 2 =',reduce(sampleArray.items, function (a, b) {return a + b}, null))
console.log('Test case 3 =',reduce([], function (a, b) {return a + b}, 5))
console.log('Test case 4 =',reduce([4], function (a, b) {return a + b}, null))