const map = require('./map')
const sampleArrays = require('./arrays')

console.log('Test case 1 =',map(sampleArrays.items,function(a){return a*2 }))
console.log('Test case 2 =',map(null,function(a){return a*2 }))
console.log('Test case 3 =',map([],function(a){return a*2 }))
