const find = require('./find')
const sampleArray = require('./arrays')

console.log('Test case 1 =',find(sampleArray.items,function (a){return a<3}))
console.log('Test case 2 =',find(sampleArray.items,function (a){return a>3}))
console.log('Test case 3 =',find(sampleArray.items,function (a){return a>10}))
console.log('Test case 4 =',find([],function (a){return a<3}))