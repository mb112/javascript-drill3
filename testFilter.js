const sampleArrays = require('./arrays')
const filter = require('./filter')


console.log('Test case 1 =',filter(sampleArrays.items,function (a){return a<3}))
console.log('Test case 2 =',filter(sampleArrays.items,function (a){return a>3}))
console.log('Test case 3 =',filter(sampleArrays.items,function (a){return a>10}))
console.log('Test case 4 =',filter([],function (a){return a<3}))