function reduce(elements, cb, startingValue = elements[0]) {

    if (Array.isArray(elements) && elements !== []) {
        if (startingValue === elements[0]) {
            for (let i = 1; i < elements.length; i++) {
                startingValue = cb(startingValue, elements[i])
            }
        } else {
            for (let i = 0; i < elements.length; i++) {
                startingValue = cb(startingValue, elements[i])

            }
        }
    }
    return startingValue
}

module.exports = reduce